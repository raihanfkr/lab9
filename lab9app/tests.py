from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.http import HttpRequest
from . import views


class lab9UnitTest(TestCase):
	def test_lab9_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_lab9_urls_is_not_exist(self):
		response = self.client.get('/')
		self.assertFalse(response.status_code==404)

	def test_lab9_login_user_exists(self):
		response = self.client.get('/')

		self.assertContains(response, 'Username')
		self.assertContains(response, 'Password')

	def test_lab9_using_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'registration/login.html')

	def test_lab9_register_user_exists(self):
		response = self.client.get('/register/')

		self.assertContains(response, 'Username')
		self.assertContains(response, 'Password')
		self.assertContains(response, 'Password confirmation')

	def test_register_user(self):
		response = self.client.post('/', follow=True, data={
			'username': 'raihanfkr',
			'password1': 'haihaihai',
			'password2': 'haihaihai',
        })


