from django.apps import AppConfig


class Lab9AppConfig(AppConfig):
    name = 'lab9app'
