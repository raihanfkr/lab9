from django.contrib import admin
from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth.views import LoginView,LogoutView

urlpatterns = [
    path('', LoginView.as_view(), name="login_url"),
    path('dashboard/', views.dashboard, name="dashboard"),
    path('register/', views.registerView, name="register_url"),
    path('logout/', LogoutView.as_view(next_page='login_url'), name="logout"),
]
